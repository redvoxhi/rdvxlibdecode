import datetime
import logging
#import multiprocessing
import os
import boto3

S3_ACCESS_KEY = os.environ.get("S3_ACCESS_KEY")
S3_SECRET_KEY = os.environ.get("S3_SECRET_KEY")
S3_DATA_BUCKET = "rdvxdata"

logging.getLogger().setLevel(logging.INFO)
logging.basicConfig(format="[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s")


# def _get((_client, _bucket, _key)):
#      _client = boto3.client('s3', aws_access_key_id=S3_ACCESS_KEY, aws_secret_access_key=S3_SECRET_KEY)
#      return _client.get_object(Bucket=S3_DATA_BUCKET, Key=_key)


class S3Client:
    def __init__(self, s3_access_key=S3_ACCESS_KEY, s3_secret_key=S3_SECRET_KEY, s3_bucket=S3_DATA_BUCKET):
        # self.conn = boto.connect_s3(s3_access_key, s3_secret_key)
        self.client = boto3.client('s3', aws_access_key_id=s3_access_key, aws_secret_access_key=s3_secret_key)
        self.bucket = s3_bucket

    def list_dir(self, fdir):
        """
        Returns a list of keys for a given "directory" on s3.
        :param fdir: A directory in the form of mic|bar/YYYY/MM/DD
        :return: A list of keys for a given directory.
        """
        logging.info("Getting list of keys from {}".format(fdir))
        d = fdir.strip("/")

        objects = self.client.list_objects(Bucket=self.bucket, Prefix=d)
        keys = map(lambda o: o["Key"], objects["Contents"])

        while objects["IsTruncated"]:
            logging.info("Resp truncated. Getting another set")
            objects = self.client.list_objects(Bucket=self.bucket, Prefix=d, Marker=keys[-1])
            keys += map(lambda o: o["Key"], objects["Contents"])

        logging.info("Found {} total keys".format(len(keys)))
        return keys
        # return self.conn.get_bucket(self.bucket).list(prefix=fdir.strip("/"))

    def search(self, start_ts_s, end_ts_s, device_ids=list()):
        """
        Returns a list of keys for mic and bar data between two timestamps and a list of device ids.
        :param start_ts_s: The start timestamp in UTC seconds since the epoch.
        :param end_ts_s:  The end timestamp in UTC seconds since the epoch.
        :param device_ids: An optional list of device ids to filter on. If this is empty, all devices are included.
        :return: A list of keys for mic and bar data that are filtered on timestamps and device ids.
        """
        logging.info("Starting search. This could take a while...")
        start = datetime.datetime.utcfromtimestamp(start_ts_s)
        end = datetime.datetime.utcfromtimestamp(end_ts_s)

        # Get a list of directories
        dirs = []
        one_day = datetime.timedelta(days=1)
        while start.day <= end.day:
            d = '{:04d}/{:02d}/{:02d}'.format(start.year, start.month, start.day)
            logging.info("Adding search dir {}".format(d))
            dirs.append(d)
            start += one_day

        # Get the contents for each directory both mic and bar
        keys = []
        for d in dirs:
            keys += self.list_dir('mic/{}'.format(d))
            keys += self.list_dir('bar/{}'.format(d))

        logging.info("Found {} total keys".format(len(keys)))

        # Filter out timestamps and device ids
        def filter_timestamps(key):
            ts = int(key.split("_")[2])
            return start_ts_s >= ts <= end_ts_s

        def filter_timestamps_devices(key):
            s = key.split("_")
            ts = int(s[2])
            device = int(s[1])
            return start_ts_s >= ts <= end_ts_s and device in device_ids

        if len(device_ids) == 0:
            # Only filter on timestamps
            filtered = filter(filter_timestamps, keys)
        else:
            # Filter on timestamps and device ids
            filtered = filter(filter_timestamps_devices, keys)

        logging.info("{} keys remain after filtering".format(len(filtered)))

        return filtered

    # def get_bytes_mt(self, keys, threads=multiprocessing.cpu_count()):
    #     pool = multiprocessing.Pool(threads)
    #
    #     partial_get = partial(_get, self.client, self.bucket)
    #
    #     pool.map(partial_get, keys)
    #     pass

    def get_bytes(self, keys):
        """
        Returns a dict of {key -> bytes}
        :param keys: List of keys to load
        :return:
        """
        logging.info("Getting bytes for {} keys".format(len(keys)))
        r = {}
        i = 1
        t = len(keys)

        for k in keys:
           logging.info("Getting {}/{} [{}%]".format(i, t, (float(i) / float(t)) * 100.0))
           r[k] = self.client.get_object(Bucket=self.bucket, Key=k)["Body"]
           i += 1

        return r

    def download(self, keys, to):
        # Make sure download directory exists, otherwise, create it
        if not os.path.isdir(to):
            logging.info("{} does not exist, attempting to create it".format(to))
            os.makedirs(to)

        # Download data
        data = self.get_bytes(keys)

        for k, v in data.iteritems():
            # Check if directory exists, if not, create it
            d = reduce(lambda l, r: l + "/" + r, k.split("/")[:-1])
            p = to + "/" + d
            if not os.path.isdir(p):
                logging.info("{} does not exists, attempting to create directory".format(p))
                os.makedirs(p)

            # Save bytes to disk
            with open(to + "/" + k, "wb") as f:
                f.write(v.read())

    def load(self, keys):
        """
        Downloads wav files to a list of in memory byte arrays.
        :param keys: List of keys to retrieve bucket items for.
        :return:
        """
        data = self.get_bytes(keys)

