import numpy as np
import struct

import BinFormat


class BinDecoder:
    """
    Constructs an instance of a decoded Redvox file with the given file name and API specification.
    """

    def __init__(self, fname, bin_format, header_only=False):
        self.fields = {}
        self.bin_format = bin_format
        self.header_only = header_only
        self.fname = fname

        # Read in all of the data from the binary file using the schema defined by the numpy dtype object.
        # This will deserialize all fields except for the final waveform array.
        fields_sans_data = np.fromfile(fname, BinFormat.get_dtype(bin_format))

        # Make all of the fields easily accessible in the fields list
        for field_name in BinFormat.get_field_names(bin_format):
            self.fields[field_name] = fields_sans_data[field_name][0]

        api = self.get("rdvxApiVersion")
        bits_per_sample = self.get("headerBitsPerSample")
        is_mic = bin_format in BinFormat.mic_formats
        is_bar = bin_format in BinFormat.bar_formats

        # Get the waveform data
        if not header_only:
            fd = open(fname, 'rb')
            fd.seek(BinFormat.get_data_offset(bin_format), 0)  # File offset at data



            # API 700
            if api == 0x700:
                if is_mic:
                    self.fields['waveform'] = np.fromfile(fd, "int" + str(bits_per_sample))
                elif is_bar:
                    self.fields['waveform'] = np.fromfile(fd, "float" + str(bits_per_sample))
                else:
                    print "Unknown bin_format " + bin_format

            # API 800
            elif api == 0x800:
                if is_mic:
                    if bits_per_sample == 16:
                        self.fields['waveform'] = np.fromfile(fd, "int" + str(bits_per_sample))
                    elif bits_per_sample == 24:
                        # 24 bit read magic from https://github.com/scipy/scipy/issues/1930#issuecomment-112812537
                        data = np.fromfile(fd, "u1")
                        a = np.empty((len(data) / 3, 4), dtype='u1')
                        a[:, :3] = data.reshape((-1, 3))
                        a[:, 3:] = (a[:, 3 - 1:3] >> 7) * 255
                        data = a.view('<i4').reshape(a.shape[:-1])
                        self.fields['waveform'] = data
                    else:
                        print "Can not handle bits per sample = " + str(bits_per_sample)

                elif is_bar:
                    self.fields['waveform'] = np.fromfile(fd, "float" + str(bits_per_sample))
                else:
                    print "Unknown bin_format " + bin_format

            # Unknown API
            else:
                print "Unknown API " + api

            fd.close()

    def get_device_id(self):
        """Returns the device id of this decoded file."""
        return self.fields['rdvxIdForVendor']

    def get_app_time_us(self):
        """Returns the app time of this decoded file."""
        return self.fields['rdvxFileStartEpoch']

    def get_waveform(self):
        """Returns the waveform of this decoded file as a numpy array"""
        return self.fields['waveform']

    def get(self, fieldName):
        """
        Helper method to easily get the value of a field stored in this BinDecoder instance.
        :param fieldName: Name of the field to extract data from. See BinFormat for list of fields names.
        :return: The value associated with this field name.
        """
        return self.fields[fieldName]

    def get_api(self):
        return self.get("rdvxApiVersion")

    def is_bar(self):
        print self.bin_format, BinFormat.bar_formats
        return self.bin_format in BinFormat.bar_formats

    def is_mic(self):
        print self.bin_format, BinFormat.bar_formats
        return self.bin_format in BinFormat.mic_formats

    def __str__(self):
        """
        Returns a string of this object which will contain the key and value of each of its fields.

        :return: A string of this object which will contain the key and value of each of its fields.
        """
        string = ""
        for field_name in BinFormat.get_field_names(self.bin_format):
            string = string + field_name + ": " + str(self.fields[field_name]) + "\n"
        string = string + "waveform: " + str(self.fields["waveform"])
        return string


# Helper functions for decoding files of a specific type
def decode_mic_700(fname, header_only=False):
    """
    Creates an instance of and decodes the passed in API 700 binary microphone data.

    :param fname: Name and location of file to decode.
    :return: An instance of decoded API 700 microphone binary file.
    """
    return BinDecoder(fname, BinFormat.MicApi700, header_only)


def decode_mic_800(fname, header_only=False):
    """
    Creates an instance of and decodes the passed in API 800 binary microphone data.

    :param fname: Name and location of file to decode.
    :return: An instance of decoded API 800 microphone binary file.
    """
    return BinDecoder(fname, BinFormat.MicApi800, header_only)


def decode_bar_700(fname, header_only=False):
    """
    Creates an instance of and decodes the passed in API 700 binary barometer data.

    :param fname: Name and location of file to decode.
    :return: An instance of decoded API 700 barometer binary file.
    """
    return BinDecoder(fname, BinFormat.BarApi700, header_only)


def decode_bar_800(fname, header_only=False):
    """
    Creates an instance of and decodes the passed in API 800 binary barometer data.

    :param fname: Name and location of file to decode.
    :return: An instance of decoded API 800 barometer binary file.
    """
    return BinDecoder(fname, BinFormat.BarApi800, header_only)


def is_mic(fname):
    s = fname.split("_")
    t = s[3]
    return t == "H" or t == "L"


def is_bar(fname):
    s = fname.split("_")
    t = s[3]
    return t == "B"


def get_api(fname):
    with open(fname, 'rb') as f:
        f.seek(BinFormat.API_OFFSET)
        return struct.unpack("i", f.read(4))[0]


def decode(fname, header_only=False):
    ismic = is_mic(fname)
    isbar = is_bar(fname)
    if not ismic and not isbar:
        print fname, "is not a mic or bar file"
        return

    api = get_api(fname)
    if api not in BinFormat.valid_apis:
        print fname, api, "is not a valid api version"
        return

    if ismic:
        if api == 0x700:
            return decode_mic_700(fname, header_only)
        elif api == 0x800:
            return decode_mic_800(fname, header_only)
        else:
            print fname, api, "not valid"
            return
    elif isbar:
        if api == 0x700:
            return decode_bar_700(fname, header_only)
        elif api == 0x800:
            return decode_bar_800(fname, header_only)
        else:
            print fname, api, "not valid"
            return
    else:
        print fname, api, "not valid"
        return