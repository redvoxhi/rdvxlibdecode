import BinUtils
import sys

class Event:
    def __init__(self, station_name, event_name, event_start, event_end):
        self.station_name = station_name
        self.event_name = event_name
        self.data_start_ts = sys.maxint
        self.data_end_ts = -sys.maxint - 1
        self.total_mic_files = 0
        self.total_bar_files = 0
        self.total_files = 0
        self.apis = set()
        self.devices = set()
        self.data = []

    def add_data(self, dir, file_ext="*.wav"):
        data = BinUtils.load_dir(dir, file_ext, header_only=True)
        ts = filter(lambda decoded: decoded.get("rdvxFileStartEpoch"), data)
        min_ts = min(ts)
        max_ts = max(ts)

        if min_ts < self.min_ts:
            self.data_start_ts = min_ts

        if max_ts > self.max_ts:
            self.data_end_ts = max_ts

        mic_files = filter(lambda decoded: decoded.is_mic(), data)
        bar_files = filter(lambda decoded: decoded.is_bar(), data)

        self.total_mic_files += len(mic_files)
        self.total_bar_files += len(bar_files)
        self.total_files = self.total_mic_files + self.total_bar_files


