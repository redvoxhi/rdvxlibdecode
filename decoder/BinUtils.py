import glob
import itertools
import numpy as np
import RedvoxDecoder

def load_dir(fdir, file_ext="*.wav", header_only=False):
    files = glob.glob(fdir + "/" + file_ext)
    return map(lambda f: RedvoxDecoder.decode(f, header_only), files)


def load_files(fdir, start_ts_s, stop_ts_s, device_ids=[]):
    """
    Loads and decodes a collection of wav files from a given directory.

    :param fdir: The directory to load wav files from.
    :param start_ts_s: The start timestamp in seconds.
    :param stop_ts_s: The stop timestamp in seconds.
    :param bin_format: The binary format used to decode the data.
    :param device_ids: A list of device ids to load data for. If deviceIds is empty, find all wav files for timestamp.
    :return: A list of dictionaries where each key is a device id, and each value is a list of ordered (by app time)
             BinDecoders.
    """
    all_wav_files = glob.glob(fdir + "/*.wav")

    def filter_filename(fname):
        timestamp = int(fname.split("_")[2])
        return start_ts_s <= timestamp <= stop_ts_s

    filtered_timestamps = filter(lambda fn: filter_filename(fn), all_wav_files)

    # If deviceIds is empty list, load all files only based on timestamps
    if len(device_ids) == 0:
        filtered = filtered_timestamps
    # Otherwise, take into consideration the device ids
    else:
        filtered = filter(lambda fn: int(fn.split("_")[1]) in device_ids, filtered_timestamps)

    # Decode the files
    decoded = map(lambda fn: RedvoxDecoder.decode(fn), filtered)
    # decoded = sorted(decoded, key=lambda bin_decoder: bin_decoder.get_device_id())
    #
    # # Group by device id
    # def group_by_id_and_sort():
    #     """
    #     Group decoded files by device id and then sort the decoded files by app time.
    #
    #     :return: A mapping from device id -> sorted list of decoded binary files.
    #     """
    #     grouped = {}
    #     for device_id, bin_decoder_list in itertools.groupby(decoded, lambda bin_decoder: bin_decoder.get_device_id()):
    #         # Ensure the the binDecoders are sorted by app time
    #         grouped[device_id] = sorted(list(bin_decoder_list), key=lambda bin_decoder: bin_decoder.get_app_time_us())
    #     return grouped

    return decoded


def concat_waveforms(bin_decoders):
    """
    Concatenate the waveforms of multiple decoded binaries.

    :param bin_decoders: List of ordered decoded binary files.
    :return: A single numpy array of all waveforms concatenated together.
    """
    waveforms = map(lambda bin_decoder: bin_decoder.get_waveform(), bin_decoders)
    return np.concatenate(waveforms)
