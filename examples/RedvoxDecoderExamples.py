# Example methods calls for working with Redvox API
from decoder import RedvoxDecoder
from decoder import BinUtils

# Load a single bar file
bar_file = RedvoxDecoder.decode("data/bar/rdvx_1000000002_1441595081_B_00.wav")

# Load a single bar file without the data chunk (wav and redvox headers only)
bar_file_header_only = RedvoxDecoder.decode("data/bar/rdvx_1000000002_1441595081_B_00.wav", header_only=True)

# Load a single mic file
mic_file = RedvoxDecoder.decode("data/mic/rdvx_1000000002_1441594812_L_00.wav")

# Load entire directory of mic files
mic_files = BinUtils.load_dir("data/mic")

# Load entire directory of mic files without data (headers only)
mic_files_header_only = BinUtils.load_dir("data/mic", header_only=True)

# Load entire directory of bar files
bar_files = BinUtils.load_dir("data/bar")

# Load entire directory of bar files without data (headers only)
bar_files_header_only = BinUtils.load_dir("data/bar", header_only=True)

# Let's access the fields from our decoded instances using helper methods
print "API", mic_file.get_api(), hex(mic_file.get_api())
print "Timestamp", mic_file.get_app_time_us()
print "Device Id", mic_file.get_device_id()
print "Waveform", mic_file.get_waveform()

# Let's access the rest of the fields from our decoded instances using the get method
print 'headerChunkId'         , mic_file.get('headerChunkId')
print 'headerChunkSize'       , mic_file.get('headerChunkSize')
print 'headerChunkFormat'     , mic_file.get('headerChunkFormat')
print 'headerSubChunkId'      , mic_file.get('headerSubChunkId')
print 'headerSubChunkSize'    , mic_file.get('headerSubChunkSize')
print 'headerAudioFormat'     , mic_file.get('headerAudioFormat')
print 'headerNumChannels'     , mic_file.get('headerNumChannels')
print 'headerSampleRate'      , mic_file.get('headerSampleRate')
print 'headerByteRate'        , mic_file.get('headerByteRate')
print 'headerBlockAlign'      , mic_file.get('headerBlockAlign')
print 'headerBitsPerSample'   , mic_file.get('headerBitsPerSample')
print 'rdvxChunkId'           , mic_file.get('rdvxChunkId')
print 'rdvxChunkSize'         , mic_file.get('rdvxChunkSize')
print 'rdvxApiVersion'        , mic_file.get('rdvxApiVersion')
print 'rdvxServerRecvEpoch'   , mic_file.get('rdvxServerRecvEpoch')
print 'rdvxIdForVendor'       , mic_file.get('rdvxIdForVendor')
print 'rdvxTruncatedUuid'     , mic_file.get('rdvxTruncatedUuid')
print 'rdvxLatitude'          , mic_file.get('rdvxLatitude')
print 'rdvxLongitude'         , mic_file.get('rdvxLongitude')
print 'rdvxAltitude'          , mic_file.get('rdvxAltitude')
print 'rdvxSpeed'             , mic_file.get('rdvxSpeed')
print 'rdvxHorizontalAccuracy', mic_file.get('rdvxHorizontalAccuracy')
print 'rdvxVerticalAccuracy'  , mic_file.get('rdvxVerticalAccuracy')
print 'rdvxCalibrationTrim'   , mic_file.get('rdvxCalibrationTrim')
print 'rdvxTimeOfSolution'    , mic_file.get('rdvxTimeOfSolution')
print 'rdvxFileStartMach'     , mic_file.get('rdvxFileStartMach')
print 'rdvxFileStartEpoch'    , mic_file.get('rdvxFileStartEpoch')
print 'rdvxNumMessageExchanges', mic_file.get('rdvxNumMessageExchanges')
print 'rdvxMessageExchanges'  , mic_file.get('rdvxMessageExchanges')
print 'rdvxSensorName'        , mic_file.get('rdvxSensorName')
print 'rdvxDeviceName'        , mic_file.get('rdvxDeviceName')
print 'rdvxSyncServer'        , mic_file.get('rdvxSyncServer')
print 'dataChunkId'           , mic_file.get('dataChunkId')
print 'dataChunkSize'         , mic_file.get('dataChunkSize')
print 'waveform'              , mic_file.get('waveform')

# Finally, let's load data from a directory based on timestamps and device ids
# This example loads all 1000000002 files from 1441595119 to 1441595427
filtered_mic_files = BinUtils.load_files("data/mic", 1441595119, 1441595427, [1000000002])
print len(filtered_mic_files)


