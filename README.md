# rdvxLibDecode

rdvxLibDecode is a Python library that provides routines and data structures necessary for deserializing microphone and 
barometer wav files generated from the [Redvox](https://itunes.apple.com/sz/artist/redvox-inc./id969566809) apps.
  
## Adding rdvxLibDecode to your project

1. Make sure you have Python 2.7.x and [numpy](http://www.numpy.org/) installed on your system.
2. Download the [source](https://bitbucket.org/redvoxhi/rdvxlibdecode/downloads) or fork/clone this repository.
3. Include the rdvxlibdecode directory in your project path
4. Reference the files in your Python project:

```
#!python

    from rdvxlibdecode.decoder import BinFormat, BinUtils. RedvoxDecoder
    # or
    import rdvxlibdecode.decoder.BinFormat
    # ... etc
```

    
## Quick Start
### BinFormat.py
This namespace contains numpy dtypes which represent an exact mapping of a particular Redvox data API. We currently
provide support for decoding API versions 700 and 800 of Redvox microphone and barometer data files.

### BinUtils.py
This namespace contains methods for filtering and loading multiple files based on Redvox device id or timestamp.

### RedvoxDecoder.py
Provides a class and static methods for decoding Redvox data.

### Code Examples
#### Loading a single .wav file
Import the `RedvoxDecoder` module and use the static `decode` method on the `BinDecoder` class.

`f = BinDecoder.decode("filename.wav")` 
will decode the wav file (microphone or barometer) and return an instance of a BinDecoder (decoded wav file).

It's possible to only decode the header information and ignore reading the data chunk of the wav files by passing `header_only=True` to the decode call as follows:
`f = BinDecoder.decode("filename.wav", header_only=True)`

#### Loading an entire directory
Import the `BinUtils` module and call the static function `load_dir`.

`files = BinUtils.load_dir("your_data_dir")` 
will return a list of decoded wav file instances.

It's also possible to load files from a directory filtered by timestamp (UTC seconds from the epoch) and device ids. You'll need the `load_files("dir", start_ts_s, end_ts_s, [device_ids])`.

Let's say we only wanted to load files from a directory between Sep 5 8AM to Sep 5 9AM only for devices 1000000001 and 1000000002.

    files = BinUtils.load_files("my_data_dir", 1444032000, 1444035600, [1000000001, 100000002])

It's important to note that the device_id list is optional. If an empty list is passed in or no devices are provided at all, then all devices between those timestamps in that directory will be loaded.

#### Working with decoded wav file instance objects
Once you have an instance (or list of instances) of RedvoxDecoders, you can access the fields of the different chunks in the wav file. Each decoded instance has a set of commonly used helper functions:

    get_device_id() # Returns device id data packet came from  
    get_app_time_us() # Returns Redvox device timestamp in microseconds  
    get_waveform() # Returns numpy array of waveform data  
    get_api() # Returns the API version for the current data file  
    is_mic() # Returns true is decoded file is a microphone file  
    is_bar() # Returns true is decoded file is a barometer file  

A decoced wav file instance also has a member function called `get("field_name")` which takes the name of the field you want to extract and returns it the value of that field for that decoded instance. As an example, you might want to get the lat and lon of a file.

    f = BinDecoder.decode("my_file.wav")
    lat = f.get("rdvxLatitude")
    lon = f.get("rdvxLongitude")

A full list of available fields for each API can be found at: https://bitbucket.org/redvoxhi/rdvxlibdecode/src/2092ac597d57dd42a062f29c184dede3182b4574/decoder/BinFormat.py?at=master&fileviewer=file-view-default

## Link to API documentation
[todo]

## Issues / Bugs / Feature Requests

If you find any issues or bugs with our software or wish to make a feature request, please fill out a ticket on our
projects [issue tracker](https://bitbucket.org/redvoxhi/rdvxlibdecode/downloads). 

For other inquiries, please contact [anthony.christe@gmail.com](anthony.christe@gmail.com).