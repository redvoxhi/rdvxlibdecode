import numpy as np

# The following are lists of tuples which describe the binary format of the wav file.
# Each tuple contains the following information:
#   (byte offset, field name, np.dtype | (np.dtype, size)
# It's important to note that np.dytpe only accepts fixed length sizes. This means the data field at the end of the
# format is really only used to store the byte offset since its tuple can not be used in the creation of a numpy
# dtype.

API_OFFSET = 44

# Microphone api 700 binary format
MicApi700 = [
    # Header chunk
    (0, 'headerChunkId', (np.str, 4)),
    (4, 'headerChunkSize', np.int32),
    (8, 'headerChunkFormat', (np.str, 4)),
    (12, 'headerSubChunkId', (np.str, 4)),
    (16, 'headerSubChunkSize', np.int32),
    (20, 'headerAudioFormat', np.int16),
    (22, 'headerNumChannels', np.int16),
    (24, 'headerSampleRate', np.int32),
    (28, 'headerByteRate', np.int32),
    (32, 'headerBlockAlign', np.int16),
    (34, 'headerBitsPerSample', np.int16),

    # Redvox chunk
    (36, 'rdvxChunkId', (np.str, 4)),
    (40, 'rdvxChunkSize', np.int32),
    (44, 'rdvxApiVersion', np.int32),
    (48, 'rdvxServerRecvEpoch', np.int64),
    (56, 'rdvxIdForVendor', np.uint32),
    (60, 'rdvxTruncatedUuid', np.int32),
    (64, 'rdvxLatitude', np.float64),
    (72, 'rdvxLongitude', np.float64),
    (80, 'rdvxAltitude', np.float64),
    (88, 'rdvxSpeed', np.float64),
    (96, 'rdvxHorizontalAccuracy', np.float64),
    (104, 'rdvxVerticalAccuracy', np.float64),
    (112, 'rdvxCalibrationTrim', np.float64),
    (120, 'rdvxTimeOfSolution', np.int32),
    (124, 'rdvxFileStartMach', np.int64),
    (132, 'rdvxFileStartEpoch', np.int64),
    (140, 'rdvxNumMessageExchanges', np.int8),
    (141, 'rdvxMessageExchanges', (np.int64, 60)),
    (621, 'rdvxSensorName', (np.str, 30)),
    (651, 'rdvxDeviceName', (np.str, 20)),
    (671, 'rdvxSyncServer', (np.str, 25)),

    # Data chunk
    (696, 'dataChunkId', (np.str, 4)),
    (700, 'dataChunkSize', np.int32),
    (704, 'waveform', (np.int16, -1))
]

MicApi800 = [
    # Header chunk
    (0, 'headerChunkId', (np.str, 4)),
    (4, 'headerChunkSize', np.int32),
    (8, 'headerChunkFormat', (np.str, 4)),
    (12, 'headerSubChunkId', (np.str, 4)),
    (16, 'headerSubChunkSize', np.int32),
    (20, 'headerAudioFormat', np.int16),
    (22, 'headerNumChannels', np.int16),
    (24, 'headerSampleRate', np.int32),
    (28, 'headerByteRate', np.int32),
    (32, 'headerBlockAlign', np.int16),
    (34, 'headerBitsPerSample', np.int16),

    # Redvox chunk
    (36, 'rdvxChunkId', (np.str, 4)),
    (40, 'rdvxChunkSize', np.int32),
    (44, 'rdvxApiVersion', np.int32),
    (48, 'rdvxServerRecvEpoch', np.int64),
    (56, 'rdvxIdForVendor', np.uint32),
    (60, 'rdvxTruncatedUuid', np.int32),
    (64, 'rdvxLatitude', np.float64),
    (72, 'rdvxLongitude', np.float64),
    (80, 'rdvxAltitude', np.float64),
    (88, 'rdvxSpeed', np.float64),
    (96, 'rdvxHorizontalAccuracy', np.float64),
    (104, 'rdvxVerticalAccuracy', np.float64),
    (112, 'rdvxCalibrationTrim', np.float64),
    (120, 'rdvxTimeOfSolution', np.int32),
    (124, 'rdvxFileStartMach', np.int64),
    (132, 'rdvxFileStartEpoch', np.int64),
    (140, 'rdvxNumMessageExchanges', np.int8),
    (141, 'rdvxMessageExchanges', (np.int64, 60)),
    (621, 'rdvxSensorName', (np.str, 30)),
    (651, 'rdvxDeviceName', (np.str, 20)),
    (671, 'rdvxSyncServer', (np.str, 25)),
    (696, 'rdvxDataServer', (np.str, 25)),

    # Data chunk
    (721, 'dataChunkId', (np.str, 4)),
    (725, 'dataChunkSize', np.int32),
    (729, 'waveform', (np.int16, -1))
]

# Barometer api 700 file format
BarApi700 = [
    # Header chunk
    (0, 'headerChunkId', (np.str, 4)),
    (4, 'headerChunkSize', np.int32),
    (8, 'headerChunkFormat', (np.str, 4)),
    (12, 'headerSubChunkId', (np.str, 4)),
    (16, 'headerSubChunkSize', np.int32),
    (20, 'headerAudioFormat', np.int16),
    (22, 'headerNumChannels', np.int16),
    (24, 'headerSampleRate', np.int32),
    (28, 'headerByteRate', np.int32),
    (32, 'headerBlockAlign', np.int16),
    (34, 'headerBitsPerSample', np.int16),

    # Redvox chunk
    (36, 'rdvxChunkId', (np.str, 4)),
    (40, 'rdvxChunkSize', np.int32),
    (44, 'rdvxApiVersion', np.int32),
    (48, 'rdvxServerRecvEpoch', np.int64),
    (56, 'rdvxIdForVendor', np.uint32),
    (60, 'rdvxTruncatedUuid', np.int32),
    (64, 'rdvxLatitude', np.float64),
    (72, 'rdvxLongitude', np.float64),
    (80, 'rdvxAltitude', np.float64),
    (88, 'rdvxSpeed', np.float64),
    (96, 'rdvxHorizontalAccuracy', np.float64),
    (104, 'rdvxVerticalAccuracy', np.float64),
    (112, 'rdvxCalibrationTrim', np.float64),
    (120, 'rdvxTimeOfSolution', np.int32),
    (124, 'rdvxFileStartMach', np.int64),
    (132, 'rdvxFileStartEpoch', np.int64),
    (140, 'rdvxNumMessageExchanges', np.int8),
    (141, 'rdvxMessageExchanges', (np.int64, 60)),
    (621, 'rdvxSensorName', (np.str, 30)),
    (651, 'rdvxDeviceName', (np.str, 20)),
    (671, 'rdvxSyncServer', (np.str, 25)),

    # Data chunk
    (696, 'dataChunkId', (np.str, 4)),
    (700, 'dataChunkSize', np.int32),
    (704, 'waveform', (np.float64, -1))
]

BarApi800 = [
    # Header chunk
    (0, 'headerChunkId', (np.str, 4)),
    (4, 'headerChunkSize', np.int32),
    (8, 'headerChunkFormat', (np.str, 4)),
    (12, 'headerSubChunkId', (np.str, 4)),
    (16, 'headerSubChunkSize', np.int32),
    (20, 'headerAudioFormat', np.int16),
    (22, 'headerNumChannels', np.int16),
    (24, 'headerSampleRate', np.int32),
    (28, 'headerByteRate', np.int32),
    (32, 'headerBlockAlign', np.int16),
    (34, 'headerBitsPerSample', np.int16),

    # Redvox chunk
    (36, 'rdvxChunkId', (np.str, 4)),
    (40, 'rdvxChunkSize', np.int32),
    (44, 'rdvxApiVersion', np.int32),
    (48, 'rdvxServerRecvEpoch', np.int64),
    (56, 'rdvxIdForVendor', np.uint32),
    (60, 'rdvxTruncatedUuid', np.int32),
    (64, 'rdvxLatitude', np.float64),
    (72, 'rdvxLongitude', np.float64),
    (80, 'rdvxAltitude', np.float64),
    (88, 'rdvxSpeed', np.float64),
    (96, 'rdvxHorizontalAccuracy', np.float64),
    (104, 'rdvxVerticalAccuracy', np.float64),
    (112, 'rdvxCalibrationTrim', np.float64),
    (120, 'rdvxTimeOfSolution', np.int32),
    (124, 'rdvxFileStartMach', np.int64),
    (132, 'rdvxFileStartEpoch', np.int64),
    (140, 'rdvxNumMessageExchanges', np.int8),
    (141, 'rdvxMessageExchanges', (np.int64, 60)),
    (621, 'rdvxSensorName', (np.str, 30)),
    (651, 'rdvxDeviceName', (np.str, 20)),
    (671, 'rdvxSyncServer', (np.str, 25)),
    (696, 'rdvxDataServer', (np.str, 25)),

    # Data chunk
    (721, 'dataChunkId', (np.str, 4)),
    (725, 'dataChunkSize', np.int32),
    (729, 'waveform', (np.float64, -1))
]

mic_formats = [MicApi700, MicApi800]
bar_formats = [BarApi700, BarApi800]
valid_apis = set([0x700, 0x800])

def get_data_offset(bin_format):
    """
    Returns the byte offset of the data field.
    :param bin_format: The binary format to extract the data offset from.
    :return: The byte offset of the data field.
    """
    return get_offset_field("waveform", bin_format)


def get_offset_field(field, bin_format):
    """
    Returns the byte offset of the specified field.
    :param field: The field to extract the byte offset for.
    :param bin_format: The binary format to extract the offset from.
    :return:
    """
    filtered = filter(lambda tuple3: tuple3[1] == field, bin_format)

    # Did not find key
    if len(filtered) == 0:
        return -1

    return filtered[0][0]


def get_dtype(bin_format):
    """
    Converts a binary format into a numpy dtype. Please note that this dtype will leave out the data field at the end
    of the binary format since dtypes can not include variable length types.
    :param bin_format: The selected binFormat to convert.
    :return: A numpy dtype.
    """
    sans_data = bin_format[:-1]
    sans_offsets = map(lambda tuple3: (tuple3[1], tuple3[2]), sans_data)
    return np.dtype(sans_offsets)


def get_field_names(bin_format):
    """
    Returns a list of field names associated with a given binary format. Does not include the data field.
    :param bin_format: Binary format to extract field names from.
    :return: List of field names associated with a given binary format.s
    """
    sans_data = bin_format[:-1]
    return map(lambda tuple3: tuple3[1], sans_data)


